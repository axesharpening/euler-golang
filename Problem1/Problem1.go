package main

import "fmt"

func main() {
 
	fmt.Printf("Problem 1: Multiples of 3 and 5\n")

	fmt.Printf("https://projecteuler.net/problem=1\n")

	fmt.Printf("If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23. Find the sum of all the multiples of 3 or 5 below 1000.\n")
	
	var iResult int = 0

	//Maybe not the most efficiant but it works
	for i := 0; i < 1000 ; i++ {
		//Check the Mod(%) for a remander and when it is Zero add the number onto the Result
		if((i%3)==0 || (i%5)==0){
			iResult += i //Increase the result
		}	
	}
 
	fmt.Printf("Result: %d \n", iResult )

}

 
